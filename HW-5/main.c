//
//
//
//  Daria Shumova
//

#include <stdio.h>
#include <mm_malloc.h>


typedef struct List
{
    char value;
    struct List *next;
} List;

typedef struct {
    List *head;
    u_int size;
    u_int maxSize;
} Stack;

typedef struct {
    List *head;
    List *tail;
    u_int size;
    u_int maxSize;
} Queue;

int seqSize = 30;

typedef enum {
    ads,
    sub,
    mult,
    divide,
    openBkt,
    closeBkt,
    operand
} operator;

//--------------- проверка на выделение памяти
void push (Stack *stack, char newValue)
{
    if (stack->size >= stack->maxSize)
        printf("Stack is full");
    else
    {
        List *temp;
        temp = (List *)malloc(sizeof(List));
        if (!temp)
            printf("malloc() returned NULL");
        else
        {
            temp->value = newValue;
            temp->next = stack->head;
            stack->head = temp;
            stack->size++;
        }
    }
}

char pop (Stack *stack)
{
    char result = 0;
    if (!stack->size)
        printf("Stack is empty");
    else
    {
        List *temp;
        temp = stack->head;
        result = stack->head->value;
        stack->head = stack->head->next;
        free(temp);
        stack->size--;
    }
    return result;
}

void printList (List *someList)
{
    while (someList)
    {
        printf("%c ", someList->value);
        someList = someList->next;
    }
}

//------------ перевод из 10 в 2 систему счисления с использованием стека
void from10to2 (int decimal)
{
    List head;
    Stack binary = {.head = &head, .size = 0, .maxSize = 100};
    while (decimal > 1)
    {
        push(&binary, (decimal % 2));
        decimal = decimal / 2;
    }
    push(&binary, decimal);
    
    printf("Число в двоичной системе счисления: ");
    while (binary.size)
        printf("%i", pop(&binary));
    printf("\n");
}

//------------- является ли введенная скобочная последовательность правильной
int isBracket (char symbol)
{
    int result = 0;
    
    if (symbol == '(' || symbol == '[' || symbol == '{')
        result = 1;
    else if (symbol == ')' || symbol == ']' || symbol == '}')
        result = 2;
    
    return result;
}

int isBktsPair (char firstBkt, char secondBkt)
{
    int result = 0;
    
    if ((firstBkt == '(' && secondBkt == ')') || (firstBkt == '[' && secondBkt == ']') || (firstBkt == '{' && secondBkt == '}'))
        result = 1;
    
    return result;
}

int isBktsSequenceRight (char *sequence)
{
    int result = 1;
    
    List head;
    Stack tempStack = {.head = &head, .size = 0, .maxSize = 100};
    
    for (int i = 0; (sequence[i] != '\0') && result; i++)
    {
        int bkt = isBracket(sequence[i]);
        if (bkt == 1)
            push(&tempStack, sequence[i]);
        else if (bkt == 2)
        {
            if (tempStack.size)
            {
                int compare = isBktsPair(pop(&tempStack), sequence[i]);
                if (!compare)
                    result = 0;
            }
            else
                result = 0;
        }
    }
    return result;
}

//------------ копирование односвязного списка
List createList (u_int lgth)
{
    List result = {.value = 0 + '0', .next = NULL};
    List *temp = (List *) malloc(sizeof(List));
    temp->value = 0 + '0';
    temp->next = NULL;
    
    if (!lgth)
        printf("Число элементов должно быть больше 0\n");
    else
    {
        for (int i = 1; i < lgth; i++)
        {
            List *next;
            next = (List *)malloc(sizeof(List));
            next->value = i + '0';
            next->next = temp;
            temp = next;
        }
        result = *temp;
    }
    return result;
}


List copyList (List originalList)
{
    List result;
    List *tempList = (List *) malloc(sizeof(List));
    tempList = &originalList;
    Stack tempStack = {.head = &result, .size = 0, .maxSize = 100};
    
    while (tempList)
    {
        push(&tempStack, tempList->value);
        tempList = tempList->next;
    }
    while (tempStack.size)
        {
            List *next;
            next = (List *)malloc(sizeof(List));
            next->value = pop(&tempStack);
            next->next = tempList;
            tempList = next;
        }
    result = *tempList;
    return result;
}

//----------------------- очередь
void enqueue (Queue *queue, char newValue)
{
    if (queue->size >= queue->maxSize)
        printf("Queue is full");
    else
    {
        List *temp;
        temp = (List *)malloc(sizeof(List));
        if (!temp)
            printf("malloc() returned NULL");
        else
        {
            if (queue->size == 0)
            {
                temp->value = newValue;
                temp->next = NULL;
                queue->head = temp;
                queue->tail = queue->head;
                queue->size++;
            }
            else
            {
                temp->value = newValue;
                temp->next = NULL;
                queue->tail->next = temp;
                queue->tail = queue->tail->next;
                queue->size++;
            }
        }
    }
}


char dequeue (Queue *queue)
{
    char result = 0;
    if (!queue->size)
        printf("Queue is empty");
    else
    {
        List *temp;
        temp = queue->head;
        result = queue->head->value;
        queue->head = queue->head->next;
        free(temp);
        queue->size--;
    }
    return result;
}

//-------------------- алгоритм перевода из инфиксной записи арифметического выражения в постфиксную
operator characterType (char c)
{
    int result;
    
    switch (c) {
        case '+':
            result = ads;
            break;
        case '-':
            result = sub;
            break;
        case '*':
            result = mult;
            break;
        case '/':
            result = divide;
            break;
        case '(':
            result = openBkt;
            break;
        case ')':
            result = closeBkt;
            break;
        default:
            result = operand;
            break;
    }
    return result;
}



Queue RPN (char *sequence)
{
    List headCA, headTX;
    Queue california = {.head = &headCA, .size = 0, .maxSize = 100};
    Stack texas = {.head = &headTX, .size = 0, .maxSize = 100};
    int flag = 1;
    char lastInTexas = '\0';
    
    for (int i = 0; sequence[i] != '\0' && flag; i++)
    {
        operator symbol = characterType(sequence[i]);
        
        if (symbol == operand)
                enqueue(&california, sequence[i]);
        else
        {
            if (!texas.size)
            {
                if (symbol == closeBkt)
                    flag = 0;
                else
                {
                    push(&texas, sequence[i]);
                    lastInTexas = sequence[i];
                }
            }
            else if (symbol == openBkt)
            {
                push(&texas, sequence[i]);
                lastInTexas = sequence[i];
            }
            else if (symbol == mult || symbol == divide)
            {
                if (characterType(lastInTexas) == mult || characterType(lastInTexas) == divide)
                {
                    enqueue(&california, pop(&texas));
                    lastInTexas = texas.head->value;
                    i--;
                }
                else
                {
                    push(&texas, sequence[i]);
                    lastInTexas = sequence[i];
                }
            }
            else if (symbol == ads || symbol == sub)
            {
                if (characterType(lastInTexas) == openBkt)
                {
                    push(&texas, sequence[i]);
                    lastInTexas = sequence[i];
                }
                else
                {
                    enqueue(&california, pop(&texas));
                    lastInTexas = texas.head->value;
                    i--;
                }
            }
            else if (symbol == closeBkt)
            {
                if (characterType(lastInTexas) == openBkt)
                {
                    pop(&texas);
                    lastInTexas = texas.head->value;
                }
                else
                {
                    enqueue(&california, pop(&texas));
                    lastInTexas = texas.head->value;
                    i--;
                }
            }
        }
    }
    if (texas.size)
    {
        if (characterType(lastInTexas) == openBkt)
            flag = 0;
        else
            enqueue(&california, pop(&texas));
    }
     if (!flag)
     {
         while (california.size)
         {
             dequeue(&california);
         }
         printf("Изначальная формула некорректно сбалансирована\n");
     }
    return california;
}

//--------------------------- main ---------------------------
int main(int argc, const char * argv[]) {
    
    char *sequence;
    u_int lght;
    List someList, anotherList;
    Queue someQueue;
    int decimal;
//----------------- 1 ----------------------
    printf("Введите число: ");
    scanf("%i", &decimal);
    from10to2(decimal);
//----------------- 3 ----------------------
    sequence = (char *)malloc(seqSize * sizeof(char));
    printf("Введите последовательность: ");
    scanf("%s", sequence);

    printf(isBktsSequenceRight(sequence) ? "Последовательность верная\n" : "Последовательность не верная\n");
    free(sequence);
 //---------------- 4 ----------------------
    printf("Введите длину списка: ");
    scanf("%i", &lght);
    someList = createList(lght);
    printf("Получаем список: ");
    printList(&someList);
 
    anotherList = copyList(someList);
    printf("\nКопируем первоначальный список: ");
    printList(&someList);
    printf("\nИ получаем: ");
    printList(&anotherList);
    printf("\n");
//----------------- 6 ----------------------
    someQueue.head = NULL;
    someQueue.size = 0;
    someQueue.maxSize = 100;
    enqueue(&someQueue, 'a');
    enqueue(&someQueue, 'b');
    enqueue(&someQueue, 'c');
    enqueue(&someQueue, 'd');
    printf("У нас есть очередь длиной %u, сама очередь: ", someQueue.size);
    printList(someQueue.head);
    printf("\n");
    printf("Достаем элемент из очереди - %c\n", dequeue(&someQueue));
    printf("У нас осталась очередь длиной %u, сама очередь: ", someQueue.size);
    printList(someQueue.head);
    printf("\n");
//------------------ 5 ---------------------
    sequence = (char *)malloc(seqSize * sizeof(char));
    printf("Введите последовательность: ");
    scanf("%s", sequence);
    
    someQueue = RPN(sequence);
    printf("Обратная польская нотация: ");
    while (someQueue.size)
        printf("%c", dequeue(&someQueue));
    printf("\n");
    
    return 0;
}
